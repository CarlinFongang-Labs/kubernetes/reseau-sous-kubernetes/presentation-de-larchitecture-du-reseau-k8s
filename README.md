# 1. Présentation de larchitecture du réseau K8s

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______


# 1. Aperçu de l'architecture réseau de Kubernetes

Nous allons fournir un aperçu de l'architecture réseau de Kubernetes. Voici un aperçu rapide des sujets que nous allons aborder :

1. Le modèle réseau de Kubernetes
2. Une vue d'ensemble de l'architecture de ce modèle réseau

# 2. Qu'est-ce que le modèle réseau de Kubernetes ?

Le modèle réseau de Kubernetes est un ensemble de normes qui définissent le comportement des communications entre les pods. Il existe diverses implémentations de ce modèle réseau, y compris le plugin réseau Calico que nous avons utilisé tout au long de ce cours. Ce que nous voulons que vous compreniez dans cette leçon, ce n'est pas une implémentation particulière du réseau Kubernetes, mais les concepts qui le sous-tendent. Quelles sont les normes qui définissent le réseau Kubernetes et comment cela fonctionne-t-il ?

# 3. L'architecture du modèle réseau de Kubernetes

Le modèle réseau de Kubernetes définit comment les pods communiquent entre eux, quel que soit le nœud sur lequel ils s'exécutent. Dans un cluster Kubernetes, nous avons plusieurs nœuds différents et chacun de ces nœuds exécute des pods. Que se passe-t-il lorsque ces pods veulent communiquer entre eux ?

L'élément le plus important à comprendre sur le modèle réseau de Kubernetes est que chaque pod a sa propre adresse IP unique dans le cluster. Cela signifie qu'un pod ne partagera pas la même adresse IP avec d'autres pods, même si ces pods sont sur un nœud différent. La raison en est que tout pod peut atteindre tout autre pod simplement en utilisant l'adresse IP du pod. Cela crée un réseau virtuel qui permet aux pods de communiquer facilement entre eux, quel que soit le nœud sur lequel ils se trouvent.

# 4. Transparence du modèle réseau Kubernetes

Lorsque vous pensez au modèle réseau de Kubernetes, vous devez comprendre que ce modèle rend le concept de nœuds Kubernetes complètement transparent. Les pods n'ont pas besoin de savoir sur quels nœuds se trouvent les autres pods pour communiquer avec eux à travers le réseau. Ils ont juste besoin de l'adresse IP du pod. Que deux pods soient sur le même nœud ou sur des nœuds complètement différents, ils peuvent communiquer entre eux de manière totalement transparente en utilisant ce réseau virtuel.

![Alt text](image.png)

# Conclusion

Pour résumer, nous avons discuté du modèle réseau de Kubernetes et de l'architecture de ce modèle. C'est tout pour cette leçon. À la prochaine !



# Reférence

https://kubernetes.io/docs/concepts/cluster-administration/networking/